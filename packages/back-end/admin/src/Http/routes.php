<?php
/**
 * Created by PhpStorm.
 * User: hasna
 * Date: 21/12/15
 * Time: 3:15 PM
 */
/* Un secure Routes | Will be available to public */
Route::group(['prefix' => 'admin', 'namespace' => 'BackEnd\Admin\Http\Controllers'], function () {
    Route::controllers(['auth' => 'AuthController']);
    Route::get('/', 'HomeController@getDashboard');
});
Route::group(['prefix' => 'admin', 'namespace' => 'BackEnd\Admin\Http\Controllers', 'middleware' => ['admin_auth','admin_menu']], function () {
    Route::get('/dashboard', array('as' => 'admin.get_home', 'uses' => 'HomeController@getDashboard'));
    Route::get('/auth/logout', array('as' => 'admin.auth.logout', 'uses' => 'AuthController@getLogout'));
    Route::get('/errors/404', array('as' => 'admin.get_404', 'uses' => 'HomeController@get404'));

    /**
     * Routes Comes here
     */
});