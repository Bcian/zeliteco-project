<?php
/**
 * Created by PhpStorm.
 * User: hasna
 * Date: 21/12/15
 * Time: 3:28 PM
 */

namespace BackEnd\Admin\Http\Controllers;


Class HomeController extends BaseController
{
    /**Return view of home page
     *
     * @return mixed
     */
    public function getHome()
    {
        return $this->view('pages.home');
    }

    /** Return view of dashboard
     *
     * @return mixed
     */
    public function getDashboard()
    {
        return $this->view('pages.dashboard');
    }

    /**Return view of 404 page
     * @return mixed
     */
    public function get404()
    {
        return $this->view('errors.404');
    }
}