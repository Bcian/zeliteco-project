<?php
/**
 * Created by PhpStorm.
 * User: hasna
 * Date: 22/12/15
 * Time: 10:44 AM
 */

namespace BackEnd\Admin\Menus;

abstract class AbstractRoleMenu
{
    abstract public function process($menu);
}