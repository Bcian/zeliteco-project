<a data-toggle="modal" href="#modal-delete-{{ $data->id }}"  class="btn btn-xs btn-danger">
    <i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i></a>

<div id="modal-delete-{{ $data->id }}" class="modal text-left fade">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['method' => 'DELETE', 'route' => ["$name.destroy", $data->id]])!!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Data</h4>
            </div>
            <div class="modal-body">
                <p>This action is irreversible, Are you sure want to delete this data?</p>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Yes', ['class' => 'btn bg-purple btn-flat']) !!}
                {!!Form::button('No', array('class' => 'btn bg-purple btn-flat','data-dismiss' => 'modal'))!!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>