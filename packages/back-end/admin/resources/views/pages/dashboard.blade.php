@extends('admin::layout.master')
@section('after-styles-end')
{!! HTML::style('back-end/admin/plugins/morris/morris.css') !!}
{!! HTML::style('back-end/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}
@section ('title', config('settings.project-name').'|Dashboard')
@endsection

@section('content')
        <!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>your content</h3>
          <p>Participants Registered</p>
        </div>
        <div class="icon">
          <i class="ion ion-bag"></i>
        </div>
        <a href="{{url('')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>your content</h3>
          <p>Sponosrs/Partners Registered</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
        <a href="{{url('')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>your content</h3>
          <p> Enquiry submitted</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="{{url()}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div><!-- ./col -->
  </div><!-- /.row -->
  <!-- Main row -->
</section><!-- /.content -->
@endsection

@section('after-scripts-end')
  {!! HTML::script('http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js'); !!}
  {!! HTML::script('back-end/admin/plugins/morris/morris.min.js'); !!}
  {!! HTML::script('back-end/admin/plugins/sparkline/jquery.sparkline.min.js'); !!}
  {!! HTML::script('back-end/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); !!}
  {!! HTML::script('back-end/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); !!}
  {!! HTML::script('back-end/admin/plugins/knob/jquery.knob.js'); !!}
  {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js'); !!}
  {!! HTML::script('back-end/admin/dist/js/pages/dashboard.js'); !!}

@endsection
