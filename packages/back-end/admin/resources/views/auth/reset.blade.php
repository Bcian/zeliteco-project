<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>e-Invento Plus</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<!-- Bootstrap 3.3.4 -->
	{!! HTML::style('css/bootstrap/css/bootstrap.min.css') !!}
	{!! HTML::style('css/main.css') !!}
	<!-- Font Awesome Icons -->
	{!! HTML::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') !!}
	<!-- Theme style -->
	{!! HTML::style('dist/css/theme.min.css') !!}
	<!-- iCheck -->
	{!! HTML::style('plugins/iCheck/square/blue.css') !!}
</head>
<body class="login-page">
	<div class="login-box">
		<div class="login-logo">
				<a href=""><b>e-Invento </b>Plus</a>

		</div><!-- /.login-logo -->
		<div class="login-box-body">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			<p class="login-box-msg">Reset Password</p>
			<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="token" value="{{ $token }}">

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Reset Password
								</button>
							</div>
						</div>
					</form>
		</div>
	</div>
	{!! HTML::script('plugins/jQuery/jQuery-2.1.4.min.js'); !!}
	{!! HTML::script('css/bootstrap/js/bootstrap.min.js'); !!}
	{!! HTML::script('plugins/iCheck/icheck.min.js'); !!}
	<script>
		$(function () {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
increaseArea: '20%' // optional
});
		});
	</script>
</body>
</html>