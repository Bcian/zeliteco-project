<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>e-Invento Plus</title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<!-- Bootstrap 3.3.4 -->
	{!! HTML::style('css/bootstrap/css/bootstrap.min.css') !!}
	{!! HTML::style('css/main.css') !!}
	<!-- Font Awesome Icons -->
	{!! HTML::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css') !!}
	<!-- Theme style -->
	{!! HTML::style('dist/css/theme.min.css') !!}
	<!-- iCheck -->
	{!! HTML::style('plugins/iCheck/square/blue.css') !!}
</head>
<body class="login-page">
	<div class="login-box">
		<div class="login-logo">
				<a href=""><b>e-Invento </b>Plus</a>
		</div><!-- /.login-logo -->
		<div class="login-box-body">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			<p class="login-box-msg">Reset Password</p>
			<form action="{{ url('/password/email') }}" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group has-feedback">
					<label class=" control-label">E-Mail Address</label>
					<input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}"/>
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat" >	Send  Reset Link</button>
					</div>
					<div class="col-xs-4">
						<a  href="{{ url('/auth/login') }}" class="btn btn-primary btn-block btn-flat" > Back to	Login</a>
					</div>
				</div>
			</form>
		</div>
	</div>
	{!! HTML::script('plugins/jQuery/jQuery-2.1.4.min.js'); !!}
	{!! HTML::script('css/bootstrap/js/bootstrap.min.js'); !!}
	{!! HTML::script('plugins/iCheck/icheck.min.js'); !!}
	<script>
		$(function () {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
increaseArea: '20%' // optional
});
		});
	</script>
</body>
</html>