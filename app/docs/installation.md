*Requirements: npm,node

1.Clone laravel admin package adminlte
2.Create a database
3.Create .env file in your root folder. Replace  database name,database username,database password,project name.
4.In terminal go to your route folder (like cd/var/www/html/project name).then do the following
	4.1 composer install.
	4.2 php artisan key:generate
			you will get  an Application key. Repace appkey in your env file with this key.
    4.3 npm install
	4.4 gulp publish-all
	4.5 gulp migrate-all
5.Create virtual host (see documenation).
6.Seed Admin seeder
    php artisan db:seed --class=AdminSeeder
7.