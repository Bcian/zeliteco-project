var elixir = require('laravel-elixir');
var gulp = require('gulp');
var shell = require('gulp-shell');
var gutil = require('gulp-util');
var merge = require('merge-stream');
var symlink = require('gulp-sym')
var wiredep = require('wiredep').stream;
var chalk = require('chalk');


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
 mix.sass('app.scss');
});


gulp.task('publish-admin', function(){
 shell.task([
  'php artisan vendor:publish --provider="BackEnd\\Admin\\AdminServiceProvider" --tag=config  --ansi',
  'php artisan vendor:publish --provider="BackEnd\\Admin\\AdminServiceProvider" --tag=seeds'
 ])
 var symlinkOperation = gulp.src(['packages/back-end/admin/public'])
     .pipe(symlink('public/back-end/admin', {force:true}));

 return merge(symlinkOperation);

});

gulp.task('migrate-admin', shell.task([
 'php artisan migrate --path=packages/back-end/admin/database/migrations --ansi',
]));

gulp.task('migrate-all', shell.task([
 'php artisan migrate --path=database/migrations --ansi',
 'php artisan migrate --path=packages/back-end/admin/database/migrations --ansi'

]));

gulp.task('publish-all', ['publish-admin'], function(){

});

