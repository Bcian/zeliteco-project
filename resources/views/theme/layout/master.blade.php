<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    @section('meta')
        <meta name="description" content="">
        <meta name="Keywords" content="">
        <!-- Twitter Card data -->
        <meta name="twitter:card" content="summary">
        <meta name="twitter:site" content=""/>
        <meta name="twitter:title" content="">
        <meta name="twitter:description" content="">
        <meta name="twitter:image" content="{!! url('');!!}">
        <!-- Open Graph data -->
        <meta property="og:title" content=""/>
        <meta property="og:site_name" content="lorem"/>
        <meta property="og:url" content="{{url()}}"/>
        <meta property="og:image" content="{!! url('');!!}"/>
        <meta property="og:description" content=""/>
    @show
    <link rel="icon" type="image/png" href="{!! url('theme/images/icon.ico');!!}">
     {!! HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css') !!}
   {{--  {!! HTML::style('css/bootstrap/css/bootstrap.min.css') !!} --}}
    {!! HTML::style('theme/css/main.css?v=2') !!}
    @stack('style')
</head>
<body data-base-url="{{url()}}">
<div class="pre-loader"></div>
@include('theme.layout.header')
{{--Yield Content--}}
@yield('content')
@include('theme.layout.footer')
{!! HTML::script('//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js'); !!}
{!! HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'); !!}

{{--	{!! HTML::script('theme/js/jquery_1.11.1.min.js')!!}--}}
{{--	{!! HTML::script('css/bootstrap/js/bootstrap.min.js') !!}--}}
{!! HTML::script('theme/js/imagesloaded.pkgd.js')!!}
{!! HTML::script('theme/js/main.js') !!}
@stack('script')
</body>
</html>